import { Angu4starterPage } from './app.po';

describe('angu4starter App', () => {
  let page: Angu4starterPage;

  beforeEach(() => {
    page = new Angu4starterPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

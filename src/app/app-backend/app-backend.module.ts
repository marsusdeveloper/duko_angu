import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UrlService } from './url/url.service';
import { RequestsService } from './requests/requests.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    RequestsService,
    UrlService
  ]
})
export class AppBackendModule { }

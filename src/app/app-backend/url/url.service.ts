import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable()
export class UrlService {
  private backendUrl;

  constructor() {
    this.backendUrl = environment.backend;
  }

  getRegistrationUrl() {
    return this.backendUrl + 'register'
  }

  getDictionary() {
    return this.backendUrl + 'dictionary'
  }

  getCategoryRelation(gender, age) {
    return `${this.backendUrl}relations/${age}/${gender}`
  }
}

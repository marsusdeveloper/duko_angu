import { Injectable } from '@angular/core';
import { UrlService } from '../url/url.service';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map'

@Injectable()
export class RequestsService {

  constructor(private http: Http, private urlService: UrlService) { }

  registerCompetitor(data) {
    const url = this.urlService.getRegistrationUrl();
    const headers = {'Content-Type': 'application/json'};

    return this.post(url, data, headers);
  }

  getDictionary() {
    const url = this.urlService.getDictionary();
    const headers = {'Content-Type': 'application/json'};

    return this.get(url, headers);
  }

  getCategoryRelation(age, gender) {
    const url = this.urlService.getCategoryRelation(gender, age);
    const headers = {'Content-Type': 'application/json'};

    return this.get(url, headers);
  }

  private get(...args): Observable<{[key: string]: any}> {
    return this.runHttp('get', args).map(response => JSON.parse(response._body));
  }

  private put(...args): Observable<{[key: string]: any}> {
    return this.runHttp('put', args);
  }

  private post(...args): Observable<{[key: string]: any}> {
    return this.runHttp('post', args);
  }

  private delete(...args): Observable<{[key: string]: any}> {
    return this.http.delete.apply(this.http, args);
  }

  private runHttp(method: string, args: Array<string>) {
    return this.http[method].apply(this.http, args);
  }
}

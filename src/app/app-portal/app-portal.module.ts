import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppBackendModule } from 'app/app-backend/app-backend.module'
import { AppUxModule } from 'app/app-ux/app-ux.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import { SponsorsComponent } from './sponsors/sponsors.component';
import { StatementComponent } from './statement/statement.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ContactComponent } from './contact/contact.component';
import { RegistrationPanelComponent } from './registration-panel/registration-panel.component';
import { RulesComponent } from './rules/rules.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    AppBackendModule,
    AppUxModule,
      ReactiveFormsModule,
      FormsModule
  ],
  declarations: [NotFoundComponent, HomeComponent, SponsorsComponent, StatementComponent, GalleryComponent, ContactComponent, RegistrationPanelComponent, RulesComponent]
})
export class AppPortalModule { }

import { Component } from '@angular/core';

@Component({
  selector: 'app-sponsors',
  templateUrl: './sponsors.component.html',
  styleUrls: ['./sponsors.component.scss']
})
export class SponsorsComponent {
  data = [
    {
      image: 'duko.png',
      title: 'Duko',
      href: 'http://duko.com.pl',
      className: 'col-xs-12 col-md-5',
      description: 'Firma DUKO Engineering sp.z o.o. od roku 2007 oferuje kompleksową realizacją inwestycji z zakresu Inżynierii Środowiska.  ' +
      'Podstawowym profilem działalności spółki DUKO Engineering jest renowacje i  wykonawstwo instalacji sieci zewnętrznych oraz specjalistycznych robót inżynierskich np. ' +
      'wypełniacze betonowe - injekty, renowacje studni oraz rurociągów, odwodnienia obiektów mostowych. Specjalizujemy się również w ' +
      'wykonawstwie oraz dostawach materiałów kanalizacji sanitarnej , ' +
      'wodociągów, zbiorników, odwodnień liniowych, separatorów, instalacji sanitarnych, chemii budowlanej.'
    },
    {
      image: 'pitbull.png',
      title: 'Pit bull',
      href: 'https://www.facebook.com/PitBullWestCoast/?fref=mentions',
      className: 'col-xs-12 col-md-5 col-md-offset-5',
      description: 'Pitbull West Coast- sklep z odzieżą sportową i uliczną dla prawdziwych warriorsów. W ofercie sklepu znajdziesz kurtki, bluzy a także kimona czy rashguardy treningowe.'
    },
    {
      image: 'jumparena.png',
      title: 'jump arena - park trampolin',
      href: 'https://www.facebook.com/jumparenapoznan/?fref=mentions',
      className: 'col-xs-12 col-md-5',
      description: 'Parki Trampolin Jump Arena to wyjątkowe miejsca na sport i rekreację. Nasze Parki posiadają udogodnienia dla wszystkich grup wiekowych, obiekty są zadaszone, całoroczne oraz ogrzewane. Parki wyposażone są w szatnie z łazienkami i przestrzenią do odpoczynku między skokami, poczekalniami oraz salkami przeznaczonymi do organizacji wszelkiego rodzaju imprez okolicznościowych. Nasze Parki to rozległy obszar trampolin z dziedzin specjalistycznych, takich jak: kosze do wsadów, ścieżki akrobatyczne, trampoliny sportowe, baseny z gąbkami,'
    }
  ];

  getLogoUrl(image) {
    return `assets/sponsors/${image}`;
  }

}

import {Component, OnInit} from '@angular/core';
import {RequestsService} from 'app/app-backend/requests/requests.service';
import {InputModel} from 'app/app-ux/app-ux-model';

@Component({
    selector: 'app-registration-panel',
    templateUrl: './registration-panel.component.html',
    styleUrls: ['./registration-panel.component.scss']
})
export class RegistrationPanelComponent implements OnInit {
    formCreationObject: InputModel[];
    list: InputModel[] = [
        {
            id: 'club_id',
            title: 'Klub',
            type: 'autocomplete',
        },
        {
            id: 'email',
            title: 'Email',
            type: 'text',
        },
        {
            id: 'name',
            title: 'Imię ',
            type: 'text',
        },
        {
            id: 'surname',
            title: 'Nazwisko ',
            type: 'text',
        },
        {
            id: 'gender',
            title: 'Płeć',
            type: 'select',
        },
        {
            id: 'age',
            title: 'Kategoria wiekowa',
            type: 'select',
        },
        {
            id: 'categories_relation_id',
            title: 'Kategoria',
            type: 'select',
            hide: true,
        },
        {
            id: 'gi',
            title: 'Formula Gi',
            value: true,
            type: 'boolean',
        },
        {
            id: 'nogi',
            title: 'Formula No-Gi',
            value: false,
            type: 'boolean',
        },
    ];
    validation = {
        status: true,
        message: ''
    };
    weightId = 0;
    u8 = 0;

    constructor(private service: RequestsService) {
    }

    ngOnInit() {
        this.service.getDictionary().subscribe((data) => {
            this.assignToFields(data, 'age');
            this.assignToFields(data, 'gender');
            this.weightId = data.common_weight_id.id;
            this.u8 = data.u8.id;
            this.list[this.findField('club_id')].options = data['club'];
            this.formCreationObject = this.list;
        });
    }

    submitForm(form) {
        if (form.valid) {
            this.service.registerCompetitor(form.value).subscribe(
                (data) => {
                    this.validation = {
                        message: 'Zawodnik zostal dodany.',
                        status: true
                    };
                },
                (error) => {
                    this.validation = {
                        message: error.status === 409 ?
                            'Zawodnik zostal juz zarejstrowany wczesniej.' :
                            'Wystapil blad, skontaktuj sie z administratorem.',
                        status: false
                    };
                });
        } else {
            this.validation = {
                message: 'Nalezy wypelnic wszystkie pola formularza.',
                status: false
            };
        }
    }

    private findField(name) {
        return this.list.findIndex((element: InputModel) => element.id === name);
    }

    private getCategoriesCallback() {
        const relationField = this.findField('categories_relation_id');
        const genderField = this.findField('gender');
        const ageField = this.findField('age');

        return () => {
            this.list[relationField].hide = !(this.list[genderField].value && this.list[ageField].value);
            if (!this.list[relationField].hide) {
                this.service.getCategoryRelation(this.list[ageField].value, this.list[genderField].value).subscribe(
                    (categoriesData: any) => {
                        this.list[relationField].options = categoriesData;
                    });
            }
        };
    }

    private assignToFields(data, name) {
        const field = this.findField(name);
        const object = {
            options: data[name],
            callback: this.getCategoriesCallback()
        };
        if (field !== -1) {
            this.list[field] = Object.assign(this.list[field], object);
        }
    }
}

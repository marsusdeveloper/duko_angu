import { Routes } from '@angular/router';

import { HomeComponent } from './app-portal/home/home.component';
import { NotFoundComponent } from './app-portal/not-found/not-found.component';
import { SponsorsComponent } from './app-portal/sponsors/sponsors.component';
import { StatementComponent } from './app-portal/statement/statement.component';
import { GalleryComponent } from './app-portal/gallery/gallery.component';
import { ContactComponent } from './app-portal/contact/contact.component';
import { RegistrationPanelComponent } from './app-portal/registration-panel/registration-panel.component';
import { RulesComponent } from './app-portal/rules/rules.component';

export const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'komunikat', component: StatementComponent},
  {path: 'sponsorzy', component: SponsorsComponent},
  {path: 'kontakt', component: ContactComponent},
  {path: 'zasady', component: RulesComponent},
  {path: 'galeria', component: GalleryComponent},
  {path: 'rejestracja', component: RegistrationPanelComponent},
  {path: '**', component: NotFoundComponent}
];

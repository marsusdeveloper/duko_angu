import {FormControl, Validators} from '@angular/forms';

export interface SelectOptionModel {
    value: boolean;
    label: string;
}

export const ValidationModel  = [
    Validators.required
];

export interface InputModel {
    id: string;
    title: string;
    type: string;
    value?: string|boolean|number;
    options?: SelectOptionModel[];
    validation?: any;
    hide?: boolean;
    valid?: boolean;
    formControl?: FormControl;
    callback?: () => void;
}

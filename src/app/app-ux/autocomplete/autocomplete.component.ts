import {Component, Input, Output, forwardRef, EventEmitter} from '@angular/core';
import {
    NG_VALUE_ACCESSOR,
    ControlValueAccessor
} from '@angular/forms';

@Component({
    selector: 'app-ux-autocomplete',
    templateUrl: './autocomplete.component.html',
    styleUrls: ['autocomplete.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => AutocompleteComponent),
            multi: true
        }
    ],
})

export class AutocompleteComponent implements ControlValueAccessor {
    @Input() options = [];
    @Output() blur = new EventEmitter();
    searchOptions = [];
    value;
    hide = true;

    writeValue(_value: any) {
        this.value = _value;
    }

    propagateChange = (_: any) => {};

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched() {
    }

    setValue(_value) {
        this.value = _value;
        this.propagateChange(_value);
    }

    setDisplay(show) {
        this.hide = !show;
    }

    filterList() {
        this.searchOptions = this.options.filter((option) => {
            return option.label.toLowerCase().indexOf(this.searchValue.toLowerCase()) !== -1;
        });
    }

    blurInput() {
        window.setTimeout(() => {
            this.setDisplay(false);
        }, 1000);
        this.blur.emit();
    }

    set listValue(value) {
        this.searchValue = value;
        this.setDisplay(false);
    }

    set searchValue(value) {
        this.setValue(value);
        if (this.searchValue.length > 1) {
            this.filterList();
        } else {
            this.searchOptions = [];
        }
    }

    get searchValue() {
        return this.value;
    }
}

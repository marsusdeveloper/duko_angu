import {Component, EventEmitter, Input, OnChanges, forwardRef} from '@angular/core';
import {
    NG_VALUE_ACCESSOR,
    ControlValueAccessor
} from '@angular/forms';
import {InputModel, SelectOptionModel} from '../app-ux-model';

@Component({
    selector: 'app-ux-input-row',
    templateUrl: './input-row.component.html',
    styleUrls: ['input-row.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => InputRowComponent),
            multi: true
        }
    ],
})

export class InputRowComponent implements OnChanges, ControlValueAccessor {
    @Input() item: InputModel;
    valid = true;

    private booleanValueOptions: SelectOptionModel[] = [
        {
            value: true,
            label: 'Tak'
        },
        {
            value: false,
            label: 'Nie'
        }
    ];

    writeValue(_value: any) {
        this.item.value = _value;
    }

    propagateChange = (_: any) => {};
    propagateTouch = () => {};

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched(fn) {
        this.propagateTouch = fn;
    }

    ngOnChanges() {
        if (this.item.type === 'boolean') {
            this.item.type = 'select';
            this.item.options = this.booleanValueOptions;
        }
    }

    onChange() {
        if (this.item.callback) {
            this.item.callback();
        }
        this.propagateChange(this.item.value);
        this.propagateTouch();
    }

    set autoCompleteValue(value) {
        this.item.value = value;
        this.onChange();
    }
}

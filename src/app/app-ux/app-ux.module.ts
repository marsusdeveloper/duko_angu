import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {InputRowComponent} from './input-row/input-row.component';
import {FormComponent} from './form/form.component';
import {ListInputComponent} from './list-input/list-input.component';
import {AutocompleteComponent} from './autocomplete/autocomplete.component';

@NgModule({
    imports: [
        RouterModule,
        BrowserModule,
        ReactiveFormsModule,
        FormsModule
    ],
    declarations: [
        FormComponent,
        InputRowComponent,
        ListInputComponent,
        AutocompleteComponent
    ],
    exports: [
        FormComponent,
        InputRowComponent,
        ListInputComponent,
        AutocompleteComponent
    ]
})
export class AppUxModule {
}

<?php

class Model
{
    protected $table;
    protected $db;
    protected $queryString;
    protected $condition = '';

    protected function prepareQuery()
    {
        $this->queryString = (string)'SELECT * FROM main.' . $this->table . ' ' . $this->condition;
    }

    protected function prepareString($string) {
        return implode(" ",
            array_map(function($element) {return ucfirst($element);}, explode(" ", $string)));
    }

    function __construct($db)
    {
        $this->db = $db;
        $this->prepareQuery();
    }

    public function getAll()
    {
        $statement = $this->db->query($this->queryString);
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function translateToSelect()
    {
        $array = [];

        foreach ($this->getAll() as $category) {
            $array[] = [
                'value' => $category['id'],
                'label' => $this->prepareString($category['name'])
            ];
        }

        return $array;
    }

    public function findBy($field, $value)
    {
        $queryString = 'SELECT * FROM ' . $this->table . ' WHERE ' . $field . ' = ' . $this->db->quote($value);

        return $this->db->query($queryString)->fetch();
    }
}

class InsertModel extends Model {

    protected $fields = [];
    protected $requestFields = [];
    protected $searchField = '';
    protected $isInserted = true;

    protected function getInsertQueryString()
    {
        $fields = implode(', ', $this->fields);
        $values = implode(', ', array_fill(0, count($this->fields), '?'));
        return "INSERT INTO " . $this->table . " (" . $fields . ") VALUES (" . $values . ")";
    }

    public function validate($data)
    {
        $fields = count($this->requestFields) > 0 ? $this->requestFields : $this->fields;

        foreach ($fields as $field) {
            if (!(isset($data[$field]) && $data[$field] !== '')) {
                return false;
            }
        }
        return true;
    }

    public function insertNew($data)
    {
        $object = $this->findBy($this->searchField, $data[$this->searchField]);
        if(!isset($object[$this->searchField])) {
            $pdoStatement = $this->db->prepare($this->getInsertQueryString());

            foreach ($this->fields as $key => $field) {
                $pdoStatement->bindParam($key + 1, $data[$field]);
            }

            $pdoStatement->execute();
            return $this->db->lastInsertId();
        }
        $this->isInserted = false;
        return $object['id'];
    }

    public function isInserted() {
        return $this->isInserted;
    }
}


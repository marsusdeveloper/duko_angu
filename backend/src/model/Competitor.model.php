<?php

class Competitor extends InsertModel
{
    protected $table = 'competitors_competitor';
    protected $fields = [
        'name',
        'gi',
        'nogi',
        'categories_relation_id',
        'club_id',
        'email'
    ];
    protected $requestFields = [
        'name',
        'surname',
        'gi',
        'nogi',
        'categories_relation_id',
        'club_id',
        'email'
    ];
    protected $searchField = 'name';

    function getInsertData($data) {
        $club = new Club($this->db);
        $data['name'] = strtolower(trim($data['name']) . ' ' . trim($data['surname']));
        $data['club_id'] = $club->insertNew($club->getInsertData($data['club_id']));
        $data['gi'] = intval($data['gi']);
        $data['nogi'] = intval($data['nogi']);
        return $data;
    }

    function dataToCsv($data) {
        $text = '';
        foreach($this->requestFields as $field) {
            $text .= $data[$field] . ';';
        }

        return $text;
    }

    function getEmail($data, $relation) {
        $txt = 'Zarejestrowano uzytkownika ' . $data['name'] . ' ' . $data['surname'] . '<br>';
        $txt .= $relation . '<br>';
        $txt .= 'W kategorii:' . ($data['gi'] ? 'GI ' : '') . '';
        $txt .= ($data['nogi'] ? 'NO-GI ' : '') . '.<br><br>';
        $txt .= '<b>Do zobaczenia na Dukocup 18 listopada!.</b>';

        return $txt;
    }
}
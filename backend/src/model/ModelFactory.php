<?php

spl_autoload_register(function($class) {
    if(strpos($class, $category = 'Category')) {
        $class = $category;
    }
    if(file_exists($classDirectory = __DIR__ . '/' . $class . '.model.php')) {
        include_once($classDirectory);
    }
});

class ModelFactory
{
    public $gender;
    public $age;
    public $category;
    public $relation;
    public $club;
    public $competitor;

    function __construct($db)
    {
        $this->category = new Category($db);
        $this->age = new AgeCategory($db);
        $this->gender = new Gender($db);
        $this->relation = new CategoryRelation($db);
        $this->club = new Club($db);
        $this->competitor = new Competitor($db);
    }
}
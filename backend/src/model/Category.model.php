<?php

class CategoryRelation extends Model
{
    protected $table = 'categories_categoriesrelation';

    function filter($age, $gender)
    {
        $this->condition = 'WHERE age_id = ' . $age . ' AND gender_id = ' . $gender;
        $this->queryString = "SELECT weight_id, c.* FROM " . $this->table . " LEFT JOIN main.categories_categories c ";
        $this->queryString .= "ON weight_id = c.id " . $this->condition;
        return $this;
    }

    function findName($id) {
        $string = 'SELECT a.name as age, w.name as weight, g.name as gender FROM categories_categoriesrelation  cr 
                    LEFT JOIN categories_categories a  ON a.id = cr.age_id 
                    LEFT JOIN categories_categories w  ON w.id = cr.weight_id 
                    LEFT JOIN categories_categories g  ON g.id = cr.gender_id 
                    WHERE cr.id = ' . $id;
        $data = $this->db->query($string)->fetch();
        return $data['gender'] . ' ' . $data['age'] . ' ' . $data['weight'];
    }
}

class CategoryModel extends Model
{
    protected $table = 'categories_categories';
    protected $type = '';

    private function getSpecificCategory($name)
    {
        $queryString = 'SELECT * FROM ' . $this->table . ' WHERE name LIKE "%' . $name . '%"';

        return $this->db->query($queryString)->fetch();
    }

    function __construct($db)
    {
        $this->condition = ' WHERE type_id = ' . $this->type;
        parent::__construct($db);
    }

    function findCommonWeightId()
    {
        return $this->getSpecificCategory('Wspolna');
    }

    function findU8Category()
    {
        return $this->getSpecificCategory('U8');
    }
}

class Gender extends CategoryModel
{
    protected $type = 3;
}

class AgeCategory extends CategoryModel
{
    protected $type = 2;
}

class Category extends CategoryModel
{
    protected $type = 1;
}
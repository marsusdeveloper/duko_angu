<?php

class Club extends InsertModel
{
    protected $table = 'club_club';
    protected $fields = [
        "name",
        "owner",
        "contact",
        "payment"
    ];
    protected $searchField = 'name';

    public function getInsertData($name, $contact = '-') {
        return [
            'name' => strtolower($name),
            'owner' => '-',
            'contact' => $contact,
            'payment' => '0'
        ];
    }
}
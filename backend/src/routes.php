<?php

use Slim\Http\Request;
use Slim\Http\Response;


$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->post('/register', function (Request $request, Response $response, array $args) {
    $data = $request->getParsedBody();
    if (!$this->model->competitor->validate($data)) {
        return $response->withStatus(404);
    }
    $this->model->competitor->insertNew($this->model->competitor->getInsertData($data));
    $status = !$this->model->competitor->isInserted() ? 409 : 201;

    if ($status === 201) {
        $relationName = $this->model->relation->findName($data['categories_relation_id']);
        $subject = '[DUKO-CUP] Rejestracja zawodnika';
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From: admin@dukocup.com.pl";
        mail($data['email'], $subject, $this->model->competitor->getEmail($data, $relationName), $headers);
        mail('dukocup@gmail.com', $subject, $this->model->competitor->datatoCsv($data), $headers);
    }
    return $response->withStatus($status);
});

$app->get('/dictionary', function (Request $request, Response $response, array $args) {
    $data = array(
        'gender' => $this->model->gender->translateToSelect(),
        'age' => $this->model->age->translateToSelect(),
        'club' => $this->model->club->translateToSelect(),
        'common_weight_id' => $this->model->category->findCommonWeightId(),
        'u8' => $this->model->category->findU8Category()
    );
    return $response->withJson($data);
});


$app->get('/relations/{age}/{gender}', function (Request $request, Response $response, array $args) {
    $route = $request->getAttribute('route');
    $age = $route->getArgument('age');
    $gender = $route->getArgument('gender');

    return $response->withJson($this->model->relation->filter($age, $gender)->translateToSelect());
});
